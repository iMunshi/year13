<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;

class ArticleController extends Controller
{
    public function index(Request $request) {
        $type = $request->get('type');

        //fetch hot or recent articles based on type
        $crawler = $this->setup_guttle('https://year13.com.au/');

        switch ($type) {
            case "hot_articles":
                $result = array(
                    "hot_articles" => $this->get_hot_articles($crawler),
                );
                break;
            case "recent_articles":
                $result = array(
                    "recent_articles" => $this->get_recent_articles($crawler),
                );
                break;
            case "all":
                $result = array(
                    "hot_articles" => $this->get_hot_articles($crawler),
                    "recent_articles" => $this->get_recent_articles($crawler),
                );
                break;
        }

        return $result;
    }

    public function search(Request $request) {
        $q = $request->get('q');

        $page = $request->get('page');

        if($page){
            $url = "https://year13.com.au/page/".$page."/?s=". $q;
        }else{
            $url = "https://year13.com.au/?s=". $q;
        }

        //search articles based on keyword
        $crawler = $this->setup_guttle($url);

        $result = array();
        $not_found = $crawler->filter("div#post-not-found")->count();
        if($not_found == 1){
            $result = array(
                "message" => $crawler->filter("div#post-not-found")->text()
            );
        }else{

            $pagination = $crawler->filter("div#main nav.cb-page-navigation ul.page-numbers li")->count();
            $page_count = 0;
            if($pagination){
                $page_count = $crawler->filter("div#main nav.cb-page-navigation ul.page-numbers li")->eq($pagination - 2)->text();
            }

            $articles = array();
            $articles = $crawler->filter("div#main article.cb-blog-style")
                ->each(function ($node){

                    $categories = $node->filter('div.cb-category > a')                               ->each(function ($node){
                                        return array(
                                            "title" => $node->text(),
                                            "url" => $node->attr('href')
                                        );
                                    });

                    return array(
                        "title" => $node->filter('h2.cb-post-title > a')->text(),
                        "url" => $node->filter('h2.cb-post-title > a')->attr('href'),
                        "img" => 'https:' . $node->filter('div.cb-mask a > img')->attr('src'),
                        "author" => $node->filter('div.cb-author > a')->text(),
                        "updated_at" => $node->filter('div.cb-date > time.updated')->text(),
                        "description" => $node->filter('div.cb-excerpt')->text(),
                        "categories" => array_slice($categories, 0, 2),
                        "tags" => []
                    );
                });

            foreach($articles as &$article){
                $article_crawler = $this->setup_guttle($article["url"]);
                $tags = $article_crawler->filter("p.cb-tags > a")
                        ->each(function ($node){
                            return array(
                                "title" => $node->text(),
                                "url" => $node->attr('href')
                            );
                        });
                $article["tags"] = array_slice($tags, 0, 2);
            }

            $result = array(
                "articles" => $articles,
                "page_count" => $page_count
            );
        }

        return $result;
    }

    public function setup_guttle($url) {
        $goutteClient = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 60,
        ));
        $goutteClient->setClient($guzzleClient);
        $crawler = $goutteClient->request('GET', $url);
        return $crawler;
    }

    public function get_hot_articles ($crawler) {
        // selecting only 4 articles
        return array_slice($this->get_articles($crawler, "hot"), 0, 4);
    }

    public function get_recent_articles ($crawler) {
        // selecting only 4 articles
        return array_slice($this->get_articles($crawler, "recent"), 0, 4);
    }
    public function get_articles ($crawler, $type) {
        if($type == "hot"){
            $filter = 'div.cb-slider-b li.cb-grid-entry';
        }else if($type == "recent"){
            $filter = 'div.cb-slider-a li.cb-grid-entry';
        }
        $result = array();
        $result =
            $crawler
            ->filter($filter)
            ->each(function ($node){
                return array(
                    "title" => $node->filter('h2.cb-post-title > a')->text(),
                    "url" => $node->filter('a')->attr('href'),
                    "img" => 'https:' . $node->filter('a > img')->attr('src'),
                    "author" => $node->filter('div.cb-author > a')->text(),
                    "updated_at" => $node->filter('div.cb-date > time.updated')->text()
                );
            });
        return $result;
    }
}
