###Problem
Create an app which fetches articles from https://year13.com.au. This repository consists of a basic scaffolding which should help get you started faster but feel free to use anything else if you're not comfortable with VueJs.

- On the home page, display hot and recent articles with any other relevant information you can fetch without sending more than one request to the server.
- If someone searches for a keyword, retrieve relevant articles and display them with pagination. You should ideally display two categories and two tags for each articles in the search result.

###Instructions
- Implement the index() and search() methods in app/Http/Controllers/ArticleController.php
- Implement the resources/assets/js/components/Article.vue component
- Make necessary changes in the resources/assets/js/pages/Home.vue component
- Make the app as close as possible to the screenshots and video below in terms of functionality
- Keep the number of http requests to the server as low as possible
- You are encouraged to do as much performance optimizations as possible, we dont expect real-time results matching the source
- Follow the existing coding and naming conventions
- Please follow your best coding practises
- Feel free to give it a personalized tuch, we wont judge!

###How to Submit
- DO NOT SEND A PULL REQUEST
- Make a patch file with all your changes and email the zipped patch file to fahad@year13.com.au
- In case you havent used the scaffolding of this reposiroty, please push it to your own repository and send us the link
- DO NOT SEND A PULL REQUEST

###Screenshots
![](https://s3-ap-southeast-2.amazonaws.com/my-year13-local/screencapture-1.jpg)

![](https://s3-ap-southeast-2.amazonaws.com/my-year13-local/screencapture-2.jpg)

###Video
![](https://s3-ap-southeast-2.amazonaws.com/my-year13-local/usage.gif)

###Installation
Run ``composer install`` and ``npm install``

Run ``npm run watch-poll`` and start coding
